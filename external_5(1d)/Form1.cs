﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace external_5_1d_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int countElem = Convert.ToInt32(numericUpDown1.Value);
            dataGridView1.Rows.Add();
            dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = countElem;

            Utils.GenerateRandomFile("Random", countElem);
            Utils.GenerateReverseFile("Back", countElem);

            TwoPhaseNaturalSort sort = new TwoPhaseNaturalSort();

            sort.SortFile("Random");
            dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = sort.countComparisons;

            sort.SortFile("Back");
            dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = sort.countComparisons;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            form.ShowDialog();
        }
    }
}
