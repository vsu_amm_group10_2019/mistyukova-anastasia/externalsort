﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace external_5_1d_
{
    public class Utils
    {
        public static int MaxNumber = 1000000;
        public static void GenerateRandomFile(string fileName, int count)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                Random random = new Random();
                for (int i = 0; i < count; i++)
                {
                    writer.Write(random.Next(MaxNumber));
                }
            }
        }

        public static void GenerateReverseFile(string fileName, int count)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                for (int i = count; i != 0; i--)
                {
                    writer.Write(i);
                }
            }
        }

        public static string PrintFile(string fileName)
        {
            string result = "";
            using (FileStream stream = File.Open(fileName, FileMode.Open))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        result += reader.ReadInt32() + ", ";
                    }
                }
            }
            return result.Trim();
        }
    }
}
